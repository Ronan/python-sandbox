from .TripDAO import TripDAO
from .UserSession import UserSession
from .UserNotLoggedInException import UserNotLoggedInException


class TripService:

    def getTripsByUser(self, user):
        logged_user = self.check_logged_user()
        if logged_user in user.getFriends():
            return self.find_trips_for_user(user)
        else:
            return []

    def check_logged_user(self):
        logged_user = self.get_logged_user()
        if not logged_user:
            raise UserNotLoggedInException()
        return logged_user

    @staticmethod
    def find_trips_for_user(user):
        return TripDAO.findTripsByUser(user)

    @staticmethod
    def get_logged_user():
        return UserSession.getInstance().getLoggedUser()


