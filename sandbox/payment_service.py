class User:
    pass


class PaymentDetails:
    pass


class InvalidUserException(Exception):
    pass


class PaymentGateway:
    def process(self, payment_details: PaymentDetails):
        pass


class UserValidator:
    def validate(self, user: User) -> bool:
        pass


class PaymentService:

    def __init__(self, gateway: PaymentGateway, validator: UserValidator):
        self.gateway = gateway
        self.validator = validator

    def process_payment(self, user: User, payment_details: PaymentDetails) -> None:
        if not self.is_valid(user):
            raise InvalidUserException()
        self.gateway.process(payment_details)

    def is_valid(self, user) -> bool:
        return self.validator.validate(user)

