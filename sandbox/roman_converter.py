class Number:
    def __init__(self, roman: str, arabic: int):
        self.roman = roman
        self.arabic = arabic


numbers = [
    Number('M', 1000),
    Number('CM', 900),
    Number('D',  500),
    Number('CD', 400),
    Number('C',  100),
    Number('XC',  90),
    Number('L',   50),
    Number('XL',  40),
    Number('X',   10),
    Number('IX',   9),
    Number('V',    5),
    Number('IV',   4),
    Number('I',    1),
]


class RomanConverter:
    @staticmethod
    def convert(arabic: int) -> str:
        roman = ''

        for number in numbers:
            while arabic >= number.arabic:
                roman = roman + number.roman
                arabic = arabic - number.arabic

        return roman
