class Year:
    def __init__(self, value) -> None:
        self.value = value

    def is_leap(self) -> bool:
        if self.is_divisible_by(400):
            return True
        elif self.is_divisible_by(100):
            return False
        elif self.is_divisible_by(4):
            return True
        else:
            return False

    def is_divisible_by(self, divisor:int) -> bool:
        return self.value % divisor == 0
