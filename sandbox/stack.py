class Stack:
    def __init__(self):
        self.content = []

    def push(self, obj):
        self.content.append(obj)

    def pop(self):
        if not self.content:
            raise EmptyStackException()
        result = self.content[-1]
        self.content = self.content[:-1]
        return result


class EmptyStackException(Exception):
    pass
