import unittest
from unittest import TestCase
from unittest.mock import MagicMock

from sandbox.trip_service.Trip import Trip
from sandbox.trip_service.TripService import TripService
from sandbox.trip_service.User import User
from sandbox.trip_service.UserNotLoggedInException import UserNotLoggedInException

unittest.TestLoader.testMethodPrefix = 'the_trip_service'


class TripServiceTest(TestCase):

    def setUp(self):
        self.bob = User()
        self.trips_for_bob = [Trip(), Trip(), Trip()]
        self.bill = User()
        self.trips_for_bill = [Trip(), Trip()]

    def the_trip_service_should_raise_an_exception_when_no_user_is_logged(self):
        service = TripService()
        service.get_logged_user = MagicMock(return_value=None)

        self.assertRaises(UserNotLoggedInException, service.getTripsByUser, None)

    def the_trip_service_should_return_no_trips_when_bob_is_logged_but_not_friend_with_bill(self):
        service = TripService()
        service.get_logged_user = MagicMock(return_value=self.bob)

        trips = service.getTripsByUser(self.bill)

        self.assertEqual([], trips)

    def the_trip_service_should_return_the_trips_of_bill_when_bob_is_logged_and_is_a_friend_of_bill(self):
        self.bill.addFriend(self.bob)

        service = TripService()
        service.get_logged_user = MagicMock(return_value=self.bob)
        service.find_trips_for_user = self.find_trips_for_user

        trips = service.getTripsByUser(self.bill)

        self.assertEqual(self.trips_for_bill, trips)

    def find_trips_for_user(self, user: User) -> list:
        if user == self.bob:
            return self.trips_for_bob
        elif user == self.bill:
            return self.trips_for_bill


