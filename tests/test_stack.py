import unittest
from unittest import TestCase

from sandbox.stack import Stack, EmptyStackException

unittest.TestLoader.testMethodPrefix = 'stack'


class TestStack(TestCase):

    def stack_should_accept_push(self):
        stack = Stack()
        self.assertIsNone(stack.push('foo'))

    def stack_should_raise_exception_on_pop_when_empty(self):
        stack = Stack()
        self.assertRaises(EmptyStackException, stack.pop)

    def stack_should_pop_last_pushed_object(self):
        stack = Stack()
        stack.push('foo')
        self.assertEqual('foo', stack.pop())

    def stack_should_pop_pushed_objects_in_reverse_order(self):
        stack = Stack()
        stack.push('foo')
        stack.push('bar')
        stack.push('qux')
        self.assertEqual('qux', stack.pop())
        self.assertEqual('bar', stack.pop())
        self.assertEqual('foo', stack.pop())
        self.assertRaises(EmptyStackException, stack.pop)
