import unittest
from unittest import TestCase

from sandbox.year import Year

unittest.TestLoader.testMethodPrefix = 'the_year'


class TestLeapYear(TestCase):

    def the_year_2000_is_leap_year_as_it_is_divisible_by_400(self):
        self.assertTrue(Year(2000).is_leap())

    def the_year_1900_is_leap_year_as_it_is_divisible_by_100_but_not_by_400(self):
        self.assertFalse(Year(1900).is_leap())

    def the_year_2016_is_leap_year_as_it_is_divisible_by_4(self):
        self.assertTrue(Year(2016).is_leap())

    def the_year_2019_is_not_leap_year_as_it_is_not_divisible_by_4(self):
        self.assertFalse(Year(2019).is_leap())
