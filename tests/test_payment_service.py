import unittest
from unittest import TestCase
from unittest.mock import MagicMock

from sandbox.payment_service import InvalidUserException, PaymentService, User, PaymentDetails, PaymentGateway, \
    UserValidator

unittest.TestLoader.testMethodPrefix = 'the_payment_service'


class TestPaymentService(TestCase):

    def setUp(self):
        self.valid_user = User()
        self.invalid_user = User()
        self.payment_details = PaymentDetails()

        self.gateway = PaymentGateway()
        self.gateway.process = MagicMock(return_value=None)

        self.validator = UserValidator()
        self.validator.validate = self.validate_user

        self.service = PaymentService(self.gateway, self.validator)

    def the_payment_service_should_throw_an_exception_when_user_is_not_valid(self):
        self.assertRaises(InvalidUserException, self.service.process_payment, self.invalid_user, self.payment_details)

    def the_payment_service_should_send_payment_to_the_gateway_when_user_is_valid(self):
        self.service.process_payment(self.valid_user, self.payment_details)

        self.gateway.process.assert_called_with(self.payment_details)

    def validate_user(self, user: User):
        if user == self.valid_user:
            return True
        else:
            return False
