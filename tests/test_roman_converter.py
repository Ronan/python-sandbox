import unittest
from unittest import TestCase

from sandbox.roman_converter import RomanConverter

unittest.TestLoader.testMethodPrefix = 'roman_converter'


class TestRomanConverter(TestCase):

    def roman_converter_should_convert_1_into_I(self):
        converter = RomanConverter()
        self.assertEqual(converter.convert(1), 'I')

    def roman_converter_should_convert_2_into_II(self):
        converter = RomanConverter()
        self.assertEqual(converter.convert(2), 'II')

    def roman_converter_should_convert_3_into_III(self):
        converter = RomanConverter()
        self.assertEqual(converter.convert(3), 'III')

    def roman_converter_should_convert_5_into_V(self):
        converter = RomanConverter()
        self.assertEqual(converter.convert(5), 'V')

    def roman_converter_should_convert_3949_into_MMMCMXLIX(self):
        converter = RomanConverter()
        self.assertEqual(converter.convert(3949), 'MMMCMXLIX')
